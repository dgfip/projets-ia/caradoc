#!/bin/bash

# Check if the size argument is provided
if [ -z "$1" ]; then
  echo "Usage: $0 <size>"
  exit 1
fi

# Assign the first argument to the size variable
SIZE=$1

curl -X PUT http://0.0.0.0:6333/collections/qdr \
-H 'Content-Type: application/json' \
--data-raw "{
  \"vectors\": {
    \"size\": $SIZE,
    \"distance\": \"Cosine\"
  },
  \"hnsw_config\": {
    \"payload_m\": 16,
    \"m\": 0
  }
}"
