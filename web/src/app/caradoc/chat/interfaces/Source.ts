export default interface Source {
    id: string
    content: string
    filename: string
    filetype: string
    index: string
    fileId: string
    score: number
}