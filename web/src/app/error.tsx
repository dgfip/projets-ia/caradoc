"use client"

import { tss } from "tss-react";
import { Button } from "@codegouvfr/react-dsfr/Button"

/**
 * Error component that is displayed when uncaught errors occur
 * @constructor
 */
export default function Error() {
    const {classes} = useStyles()
    return (
        <div className={classes.container}>
            <div className={classes.innerContainer}>
                <h1>Erreur</h1>
                <p>Oops, une erreur est survenue !</p>
                <p>Nous en sommes désolés !</p>
                <Button priority="primary"
                        iconId="ri-home-7-line"
                        iconPosition="right"
                        linkProps={{
                            href: '/',
                        }}>
                    Revenir à l'accueil
                </Button>
            </div>
        </div>
    );
}

const useStyles = tss
    .create(() => ({
        container: {
            display: "flex",
            width: "100vw",
            height: "100vh",
        },
        innerContainer: {
            transform: "translateY(-50px)",
            margin: "auto",
            textAlign: 'center',
        }
    }))