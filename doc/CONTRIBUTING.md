# Comment contribuer au projet CARADOC ? 

## Méthodes de contribution 

Il existe différentes méthodes pour contribuer à CARADOC :
- Documentation : Ajout de remarques, d'installation dans des cas particuliers, amélioration générale de la documentation;
- Code : Ajout de fonctionnalités, correction de bugs, optimisation de fonctionnalités
- Échanges : Discussion autour de nouvelles fonctionnalités, de problèmes techniques éventuels.

Il est possible d'utiliser les issues Gitlab pour faire remonter les questions, les problèmes techniques...

Par ailleurs, pour intégrer du code, de la documentation, les bonnes pratiques Gitlab sont fortement conseillées : création d'une branche avec un nom explicite, création d'une merge request détaillée. 

## Installation environnement de développement 

### Pré-requis 

Pour développer avec CARADOC, il sera nécessaire d'installer localement : 
- [skaffold](https://skaffold.dev/docs/install/) 
- [minikube](https://kubernetes.io/fr/docs/tasks/tools/install-minikube/) (ou solution équivalente)
- [kubectl](https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/) 
- [python3.10](https://www.python.org/downloads/)
- [pipenv](https://pipenv.pypa.io/en/latest/installation.html)
- [node 20](https://nodejs.org/en/download/package-manager)

Ensuite, il sera important d'installer les dépendances pour la partie web :

```bash
npx create-next-app@latest
npm install @codegouvfr/react-dsfr
```

De même pour la partie API :

```
pipenv install
```

### Lancement de l'application

```bash
minikube start --driver docker --static-ip APP_STATIC_IP
minikube addons enable ingress
kubectl label nodes minikube size=large
docker compose -f docker_compose/docker_compose_caradoc.yml up -d qdrant caradoc-dev # Launch external components
docker compose -f docker_compose/external_components/docker_compose_mlflow.yml up -d # Launch external components 
skaffold dev
```

### Structure du dépôt

```bash
.
├── api # API code repository 
│   ├── ai_models # AI model folder
│   ├── app # source folder
│   ├── test # Folder containing test scripts
├── doc # Documentaiton folder
├── docker_compose # folder containing files to run docker compose deployment
│   ├── external_components # external services to run application
│   ├── nginx #nginx configuration
├── k8s # Kubernetes et Helm deployment
│
└── web # Web code repository

```

