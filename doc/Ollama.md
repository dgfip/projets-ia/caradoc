# Ollama 

Ollama est une solution pour déployer en local des modèles de langages (LLM / Embeddings).

## Installation et configuration (MacOS)

En premier lieu, il suffit d'installer Ollama sur le [site officiel](https://ollama.com).

Dès lors, nous configurerons Ollama afin qu'il écoute sur tous les ports réseaux de notre machine :

```bash
launchctl setenv OLLAMA_HOST 0.0.0.0:8080 
```

La commande suivante permet de lancer le serveur Ollama :

```bash
ollama serve
```

Pour télécharger un modèle, nous ferons par exemple :

```bash
ollama pull mistral
```

Une fois téléchargé, le serveur Ollama lancera les modèles en mémoire en fonction des requêtes reçues par les utilisateurs. 
