FROM python:3.10.13-slim

ARG http_proxy_arg
ARG https_proxy_arg

ENV HTTP_PROXY=${http_proxy_arg:-""}
ENV HTTPS_PROXY=${https_proxy_arg:-""}
RUN set -eux; \
    if [ -n "${http_proxy_arg}" ]; then \
      echo "[global]" > pip.conf; \
      echo "proxy=${HTTP_PROXY}" >> pip.conf; \
    fi; \
    \
    \
    touch /etc/apt/apt.conf; \
    if [ -n "$http_proxy_arg" ]; then \
      echo 'Acquire::http::proxy "$HTTP_PROXY";' >> /etc/apt/apt.conf; \
      sed -i "s|\$HTTP_PROXY|${HTTP_PROXY}|g" /etc/apt/apt.conf; \

    fi; \
    if [ -n "$https_proxy_arg" ]; then \
      echo 'Acquire::https::proxy "$HTTPS_PROXY";' >> /etc/apt/apt.conf; \
      sed -i "s|\$HTTPS_PROXY|${HTTPS_PROXY}|g" /etc/apt/apt.conf; \
    fi; \
    find /etc/apt/ -maxdepth 1 -size 0c -name "apt.conf" -delete

ENV PYTHONUNBUFFERED 1
# We disable bytecode generation for cleaner build
ENV PYTHONDONTWRITEBYTECODE 1

ENV PIP_CONFIG_FILE=pip.conf

COPY requirements.txt .

RUN apt-get update \
 && apt-get install gnupg ffmpeg libsm6 libxext6 pandoc enchant-2 myspell-fr-fr -y \
 && pip install --no-cache-dir --upgrade -r requirements.txt \
 && python3 -m nltk.downloader averaged_perceptron_tagger

WORKDIR /app

COPY . .

CMD ["uvicorn", "app.main:app", "--root-path", "/api","--reload", "--host", "0.0.0.0", "--port" , "8100"]
