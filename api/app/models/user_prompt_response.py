from typing import List

from pydantic import BaseModel


class Source(BaseModel):
    id: str
    content: str
    filename: str
    filetype: str
    index: str
    file_id: str
    score: float


class UserPromptResponse(BaseModel):
    sources: List[Source] | None = None
    content: str | None = None
    generation_completed: bool | None = None
