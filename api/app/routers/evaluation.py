from typing import Dict

from fastapi import APIRouter, Depends
from qdrant_client.http import models

import app.ds.rag_pipeline as rag
from app.config.qdrant import BASE_COLLECTION_NAME
from app.config.qdrant import client as qdrant_client
from app.config.rag import MODELS, PRECISION
from app.dependencies.ai_models import get_eval_message_type_model
from app.ds.eval_pipeline import CaradocEvalPipeline
from app.models.app.success_response import SuccessResponse
from app.models.eval_pipeline_request import EvalPipelineRequest

router = APIRouter(
    prefix="/evaluation",
    tags=["evaluation"],
)


@router.post(
    "/eval",
    response_model=SuccessResponse[Dict[str, float]],
    response_description="Compute RAG evaluation metrics",
)
def eval_pipeline_on_collection(
        eval_request: EvalPipelineRequest, clf_pr=Depends(get_eval_message_type_model)
) -> SuccessResponse[Dict[str, float]]:
    """This function allows to eval a workflow (pipeline RAG) on a collection database

    Args:
        eval_request (EvalPipelineRequest): Request as a EvalPipelineRequest object
        clf_pr (_type_, optional): RAG request classifier. Defaults to Depends(get_eval_message_type_model).

    Returns:
        SuccessResponse[Dict[str, float]]: RAG evaluation metrics
    """

    index = eval_request.index
    workflow = eval_request.workflow
    precision = PRECISION
    params = {"workflow": workflow, "index": index, "precision": precision}
    # Get data
    data = qdrant_client.scroll(
        collection_name=BASE_COLLECTION_NAME,
        scroll_filter=models.Filter(
            must=[
                models.FieldCondition(
                    key="index", match=models.MatchValue(value=index)
                ),
            ]
        ),
        limit=100000,
        with_payload=True,
        with_vectors=False,
    )[0]
    texts = [d.payload["text"] for d in data]
    ids = [d.id for d in data]
    # Get workflow
    filters = {"index": index}
    rag_pipeline = rag.get_rag_pipeline(
        workflow=workflow,
        collection_name=BASE_COLLECTION_NAME,
        model_names=MODELS,
        filters=filters,
    )

    # Launch evaluation

    return SuccessResponse(
        data=CaradocEvalPipeline(
            params=params,
            ids=ids,
            texts=texts,
            generation_model=MODELS["eval_generation_llm"],
            judge_model=MODELS["llm_judge"],
            embed_model=MODELS["embed_model"],
            rag_pipeline=rag_pipeline,
            clf_pr=clf_pr,
            collection_name=BASE_COLLECTION_NAME
        ).eval_pipeline()
    )
