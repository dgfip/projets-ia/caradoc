## Références

### qdrant
https://artifacthub.io/packages/helm/qdrant/qdrant


### redis (bitnami)
https://artifacthub.io/packages/helm/bitnami/redis


### minio (bitnami)
https://artifacthub.io/packages/helm/bitnami/minio


### mongodb (bitnami)
https://artifacthub.io/packages/helm/bitnami/mongodb

https://github.com/bitnami/containers/blob/main/bitnami/mongodb/README.md


### postgresql (bitnami)
https://artifacthub.io/packages/helm/bitnami/postgresql


### mlflow (bitnami)
https://artifacthub.io/packages/helm/bitnami/mlflow

